# coding=utf-8
from flask import jsonify
from validate_email import validate_email
from app.models import Users, UsersApiTokens
from datetime import datetime
import socket
# TODO: Создать отдельный метод на каждый success
# TODO: Создать комментарии на каждый метод


# TODO: Генерация токена и кдов
# Генерация данных
class Generate(object):
    @staticmethod
    def token():
        return "abcdef123467890"

    @staticmethod
    def code(type):
        if "phone" == type:
            return "011235"
        elif "email" == type:
            return "qwerty0987654321"
        else:
            return None

# Отправка сообщений
class SendMessage(object):
    @staticmethod
    def register_company(user_id):
        # TODO: Уведомление администрации о запросе на регистрацию юр. лица
        return user_id

    @staticmethod
    def verify_phone(phone, code):
        # TODO: Отправка SMS для подтверждения
        return phone + code

    @staticmethod
    def verify_email(email, code):
        # TODO: Отправка письма для подтверждения
        return email + code


# Проверки данных для API
class Validate(object):
    @staticmethod
    def email_exists(email):
        if email is not None:
            return Users.query.filter_by(email=email).first()
        return False

    @staticmethod
    def phone_number_exists(phone_number):
        if phone_number is not None:
            return Users.query.filter_by(phone_number=phone_number).first()
        return False

    @staticmethod
    def email(email):
        if email is not None:
            return validate_email(email, check_mx=False, verify=False)
        return False

    @staticmethod
    def phone_number(phone_number):
        if phone_number is not None:
            return phone_number.is_integer()
        return False

    @staticmethod
    def user_verified(uid):
        if phone_number is not None:
            user =  Users.query.get(uid)
            if user is not None and user is not False:
                return user.is_email_verified and user.is_phone_number_verified
        return False

    @staticmethod
    def ip(first_ip):
        if first_ip is not None:
            try:
                socket.inet_aton(first_ip)
                return True
            except socket.error:
                return False
        return False

    @staticmethod
    def token(app_version, token):
        if app_version is not None and token is not None:
            if "android" == app_version and "ios" == token:
                return True
        return False

    @staticmethod
    def user(uid, password):
        if uid is not None and password is not None:
            user = Users.query.get(uid)
            if user is not None:
                return password == user.password
        return False

    @staticmethod
    def api_token(app, token):
        if token is not None and app is not None:
            t = UsersApiTokens.query.filter_by(app_id=app).first()
            if t is not None and token == t.token:
                return t.user_id
        return False

    @staticmethod
    def email_token(email, token):
        if email is not None and token is not None:
            t = UserVerifyEmail.query.filter_by(email=email).first()
            if t is not None and datetime.utcnow() <= t.expires_at:
                if token == t.token:
                    return t.user_id
        return False

    @staticmethod
    def phone_token(phone_number, token):
        if phone_number is not None and token is not None:
            t = UserVerifyPhone.query.filter_by(phone_number=phone_number).first()
            if t is not None and datetime.utcnow() <= t.expires_at:
                if token == t.token:
                    return t.user_id
        return False


# Формирование сообщения об успехе для API
class SuccessMessage(object):
    response = {
        "result": "success",
        "code": 200,
        "description": None,
        "data": {
            "count": 0,
            "data": []
        }
    }

    def user_add(self, data):
        self.response["description"] = "User added successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def user_login(self, data):
        self.response["description"] = "User logged in successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def user_update(self, data):
        self.response["description"] = "User profile updated successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def user_token_renew(self):
        self.response["description"] = "User token renewed successfully"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    def verify_user_email(self):
        self.response["description"] = "User email was successfully verified"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    def verify_user_phone(self):
        self.response["description"] = "User phone number was successfully verified"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    def user_get(self, data):
        self.response["description"] = "User profile found successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def config_get(self, data):
        self.response["description"] = "User config found successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def config_update(self, data):
        self.response["description"] = "User config updated successfully"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def one(self, description, data):
        self.response["description"] = description
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def many(self, description, count, data):
        self.response["description"] = description
        self.response["data"]["count"] = count
        self.response["data"]["data"].extend(data)
        return self.response


# Формирование сообщения об ошибке для API
class ErrorMessage(object):
    response = {
        "result": "error",
        "code": 0,
        "description": None,
        "data": {
            "count": 0,
            "data": []
        }
    }

    def no_data(self):
        self.response["code"] = -1
        self.response["description"] = "No data passed"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    def user_login(self):
        self.response["code"] = -2
        self.response["description"] = "Incorrect login or password"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []

    def email_exists(self, data):
        self.response["code"] = -3
        self.response["description"] = "Users with this email already exists"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def phone_number_exists(self, data):
        self.response["code"] = -4
        self.response["description"] = "Users with this phone number already exists"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append(data)
        return self.response

    def no_email(self, email):
        self.response["code"] = -5
        self.response["description"] = "No email passed"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append({"email": email})
        return self.response

    def no_phone_number(self, phone_number):
        self.response["code"] = -6
        self.response["description"] = "No phone number passed"
        self.response["data"]["count"] = 1
        self.response["data"]["data"].append({"phone_number": phone_number})
        return self.response

    def user_unverified(self):
        self.response["code"] = -7
        self.response["description"] = "User email or phone number unverified"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    # HTTP-коды
    #   Главная ошибка протокола
    def main(self):
        self.response["code"] = 418
        self.response["description"] = "I'm a teapot"
        self.response["data"]["count"] = -1
        self.response["data"]["data"].append({"HTCPCP": True})

    #   Ошибки клиента
    #       Объект не найден
    def c404(self):
        self.response["code"] = 404
        self.response["description"] = "Not found"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    #       Доступ для этого пользователя запрещён
    def c403(self):
        self.response["code"] = 403
        self.response["description"] = "Forbidden"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    #       Неавторизован
    def c401(self):
        self.response["code"] = 401
        self.response["description"] = "Unauthorized"
        self.response["data"]["count"] = 0
        self.response["data"]["data"] = []
        return self.response

    #   Ошибки сервера
    #       API не реализовано
    def c501(self):
        self.response["code"] = 501
        self.response["description"] = "Not Implemented"
        methods = [
            "user.add",
            "user.verify.email",
            "user.verify.phone",
            "user.login.email",
            "user.login.phone",
            "user.profile.get",
            "user.profile.update",
            "user.config.get",
            "user.config.update"
        ]
        self.response["data"]["count"] = len(methods)
        self.response["data"]["data"].extend(methods)
        return self.response
