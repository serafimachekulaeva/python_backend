from app import ma


class EmployerSchema(ma.Schema):
    class Meta:
        fields = ('employer_id', 'first_name', 'middle_name', 'last_name', 'email', 'is_email_verified',
                  'phone_number', 'avatar_pic', 'avatar_thumbs', 'department_id', 'group_id', 'is_read_only',
                  'created_at', 'updated_at')


class UserSchema(ma.Schema):
    class Meta:
        fields = ('user_id', 'user_type', 'user_type_id', 'display_name', 'avatar_pic', 'avatar_thumbs', 'bio',
                  'orders_done', 'orders_in_progress', 'orders_created', 'verification')


# Инициализируем схемы
employer_schema = EmployerSchema(strict=True)  # Если возвращается один объект
employers_schema = EmployerSchema(many=True, strict=True)  # Еесли возвращается несколько объектов
user_schema = UserSchema(strict=True)