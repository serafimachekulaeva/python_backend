# -*- coding: utf-8 -*-

from flask import request
from app import app
from app.models import *
from app.schemas import *
from functions import *
# TODO: Обрабокта стандартных ошибок HTTP (401, 403, 404, 503)
# TODO: Проверить возврат из БД (existing_user & existing_user_id)
# TODO: Проверять на запросах подтверждение почты и телефона
# TODO: Проверить везде self (1/2 args)

# Инициализируем пользовательские классы
gen = Generate()
send = SendMessage()
check = Validate()
error = ErrorMessage()
success = SuccessMessage()

# API version: 0.2.0
#   Создание нового пользователя
@app.route('/api/v0.2.0/user.add', methods=['POST'])
def add_user():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка на валидность email
    if not check.email(request.json['email']):
        return error.no_email(email=request.json['email'])
    # Проверка на валидность номера телефона
    if not check.phone_number(request.json['phone_number']):
        return error.no_phone_number(phone_number=request.json['phone_number'])
    # Проверка на наличие пользователя с таким email
    existing_user_id = check.email_exists(request.json['email'])
    if existing_user_id is not None or existing_user_id is not False:
        existing_user = Users.query.get(existing_user_id)
        data = {
            "user_id": existing_user.id,
            "is_company": existing_user.is_company,
            "created_at": existing_user.created_at
        }
        return error.email_exists(data=data)
    # Проверка на наличие пользователя с таким номером телефона
    existing_user_id = check.phone_number_exists(request.json['phone_number'])
    if existing_user_id is not None or existing_user_id is not False:
        existing_user = Users.query.get(existing_user_id)
        data = {
            "user_id": existing_user.id,
            "is_company": existing_user.is_company,
            "created_at": existing_user_id.created_at
        }
        return error.phone_number_exists(data=data)
    # Создание пользователя
    new_user = Users(email=request.json['email'],
                     phone_number=request.json['phone_number'],
                     password=request.json['password'],
                     is_email_verified=True,
                     is_phone_number_verified=True,
                     is_company=request.json['is_company'])
    db.session.add(new_user)
    db.session.commit()
    new_user = Users.query.filter_by(email=request.json['email']).first()
    # Проверка на получение данных из БД
    if False == new_user or None == new_user:
        return error.main()
    # Создание профиля пользователя
    new_user_profile = UsersProfiles(id=new_user.id,
                                     display_name=request.json['display_name'],
                                     pic=request.json['pic'],
                                     thumbs=request.json['thumbs'],
                                     bio=request.json['bio'],
                                     is_data_verified=False)
    db.session.add(new_user_profile)
    db.session.commit()
    # Обработка запроса на регистрацию компании
    if request.json['is_company']:
        send.register_company(new_user.id)
    # TODO: Подтверждение адреса электронной почты (в т.ч. True на False)
    # TODO: Подтверждение номера телефона (в т.ч. True на False)
    # Подготовка данных для ответа
    new_user_profile = UsersProfiles.query.get(new_user.id)
    # Проверка на получение данных из БД
    if False == new_user_profile or None == new_user_profile:
        return error.main()
    data = {
        "user_id": new_user.id,
        "display_name": new_user_profile.display_name,
        "pic": new_user_profile.pic,
        "thumbs": new_user_profile.thumbs,
        "phone_number": new_user.phone_number,
        "email": new_user.email,
        "created_at": new_user.created_at
    }
    # Отправка ответа
    return success.user_add(data)

#   Логин пользователя
@app.route('/api/v0.2.0/user.login', methods=['POST'])
def login_user():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка типа логина
    if 'email' == request.json['login_type']:
        # Проверка на валидность email
        if not check.email(request.json['email']):
            return error.no_email(request.json['email'])
        # Проверка на существование пользователя с таким email
        user = check.email_exists(request.json['email'])
        if None == user or False == user:
            return error.c404()
    elif 'phone' == request.json['login_type']:
        # Проверка на валидность номера телефона
        if not check.phone_number(request.json['phone_number']):
            return error.no_phone_number(phone_number=request.json['phone_number'])
        # Проверка на существование пользователя с таким номером телефона
        user = check.phone_number_exists(request.json['phone_number'])
        if None == user or False == user:
            return error.c404()
    else:
        return error.no_login_type
    # Проверка подтверждения регистрации
    if not check.user_verified(user.id):
        return error.user_unverified()
    # Проверка пароля
    if not check.user(user.id, password):
        return error.user_login()
    # Поиск существующего токена и обновление или добавление нового
    existing_token = UsersApiTokens.query.filter_by(app_id=request.json['app_id']).first()
    if existing_token is not None or existing_token is not False:
        new_token = UsersApiTokens(id=existing_token.id,
                                   user_id=user.id,
                                   app_id=request.json['app_id'],
                                   token=gen.token())
    else:
        new_token = UsersApiTokens(user_id=user.id,
                                   app_id=request.json['app_id'],
                                   token=gen.token())
    db.session.add(new_token)
    db.session.commit()
    # Подготовка данных для ответа
    new_token = UsersApiTokens.query.filter_by(app_id=request.json['app_id']).first()
    # Проверка на получение данных из БД
    if False == new_token or None == new_token:
        return error.main()
    data = {
        "user_id": user.id,
        "api_token": new_token.token,
        "token_expires_at": new_token.expires_at
    }
    # Отправка ответа
    return success.user_login(data)

#   Профиль пользователя
#       Отправка данных
@app.route('/api/v0.2.0/user.profile.get', methods=['POST'])
def get_user_profile():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка токена
    user_id = check.api_token(request.json['app_id'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Проверка соответствия пользователя
    if not user_id == request.json['user_id']:
        return  error.c403()
    # Проверка подтверждения регистрации
    if not check.user_verified(user_id):
        return error.user_unverified()
    # Подготовка данных для ответа
    user_profile = UsersProfiles.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_profile or None == user_profile:
        return error.c404()
    data = {
        "display_name": user_profile.display_name,
        "pic": user_profile.pic,
        "thumbs": user_profile.thumbs,
        "bio": user_profile.bio,
        "is_data_verified": user_profile.is_data_verified,
        "created_at": user_profile.created_at,
        "tests_and_certificates": False,
        "portfolios": False
    }
    if user_profile.tests_and_certificates is not None and user_profile.tests_and_certificates is not False:
        data['tests_and_certificates'] = True
    if user_profile.portfolios is not None and user_profile.portfolios is not False:
        data['portfolios'] = True
    # Отправка ответа
    return success.user_get(data)

#       Обновление данных
@app.route('/api/v0.2.0/user.profile.update', methods=['POST'])
def update_user_profile():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка токена
    user_id = check.api_token(request.json['app_id'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Проверка соответствия пользователя
    if not user_id == request.json['user_id']:
        return error.c403()
    # Проверка подтверждения регистрации
    if not check.user_verified(user_id):
        return error.user_unverified()
    # Поиск пользователя по id
    user_profile = UsersProfiles.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_profile or None == user_profile:
        return error.c404()
    # FIXME: Костыль для обработки непоступивших значений для обновления
    new_data=False
    if request.json['display_name'] is not None:
        display_name = request.json['display_name']
        new_data = True
    else:
        display_name = user_profile.display_name
    if request.json['pic'] is not None and request.json['thumbs'] is not None:
        pic = request.json['pic']
        thumbs = request.json['thumbs']
        new_data = True
    else:
        pic = user_profile.pic
        thumbs = user_profile.thumbs
    if request.json['bio'] is not None:
        bio = request.json['bio']
        new_data = True
    else:
        bio = user_profile.bio
    # Проверка на получение новых данных
    if not new_data:
        return error.no_data()
    user_profile = UsersProfiles(id=user_id,
                                 display_name=display_name,
                                 pic=pic,
                                 thumbs=thumbs,
                                 bio=bio)
    db.session.add(user_profile)
    db.session.commit()
    # Подготовка данных для ответа
    user_profile = UsersProfiles.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_profile or None == user_profile:
        return error.main()
    data = {
        "display_name": user_profile.display_name,
        "pic": user_profile.pic,
        "thumbs": user_profile.thumbs,
        "bio": user_profile.bio
    }
    # Отправка ответа
    return success.user_update(data)

#   Подтверждение логинов
#       Адреса электронной почты
@app.route('/api/v0.2.0/user.verify.email', methods=['GET, POST'])
def verify_user_email():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на валидность адреса электронной почты
    if not check.email(request.json['email']):
        return error.no_email(email=request.json['email'])
    # Проверка на задачу-обновление токена
    if request.json['renew']:
        # Получение данные текущего токена
        token = UserVerifyEmail.query.filter_by(email=request.json['email']).first()
        # Проверка на получение данных из БД
        if False == token or None == token:
            return error.c404()
        # Генерация новый токен
        code = gen.code('email')
        if None == code or False == code:
            return error.main()
        send.verify_email(request.json['email'], code)
        # Добавление в БД нового токена
        token = UserVerifyEmail(id=token.id,
                                email=request.json['email'],
                                token=code)
        db.session.add(token)
        db.session.commit()
        # Отправка ответа:
        return success.user_token_renew()
    # Проверка токена
    user_id = check.email_token(request.json['email'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Подтверждение почты в БД
    user = Users(id=user_id,
                 is_email_verified=True)
    db.session.add(user)
    db.session.commit()
    # Отправка ответа
    return success.verify_user_email()

#       Номера телефона
@app.route('/api/v0.2.0/user.verify.phone', methods=['POST'])
def verify_user_phone():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на валидность номера телефона
    if not check.phone_number(request.json['phone_number']):
        return error.no_phone_number(phone_number=request.json['phone_number'])
    # Проверка на задачу-обновление токена
    if request.json['renew']:
        # Получение данные текущего токена
        token = UserVerifyPhone.query.filter_by(phone_number=request.json['phone_number']).first()
        # Проверка на получение данных из БД
        if False == token or None == token:
            return error.c404()
        # Генерация новый токен
        code = gen.code('phone')
        if None == code or False == code:
            return error.main()
        send.verify_phone(request.json['phone_number'], code)
        # Добавление в БД нового токена
        token = UserVerifyPhone(id=token.id,
                                phone_number=request.json['phone_number'],
                                token=code)
        db.session.add(token)
        db.session.commit()
        # Отправка ответа:
        return success.user_token_renew()
    # Проверка токена
    user_id = check.phone_token(request.json['phone_number'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Подтверждение номера в БД
    user = Users(id=user_id,
                 is_phone_number_verified=True)
    db.session.add(user)
    db.session.commit()
    # Отправка ответа
    return success.verify_user_phone()


#   Настройки пользователя
#       Отправка данных
@app.route('/api/v0.2.0/user.config.get', methods=['POST'])
def get_user_config():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка токена
    user_id = check.api_token(request.json['app_id'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Проверка соответствия пользователя
    if not user_id == request.json['user_id']:
        return error.c403()
    # Проверка подтверждения регистрации
    if not check.user_verified(user_id):
        return error.user_unverified()
    # Подготовка данных для ответа
    user_config = UsersConfig.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_config or None == user_config:
        return error.c404()
    data = {
        "theme": user_config.theme,
        "locale": user_config.locale
    }
    # Отправка ответа
    return success.config_get(data)


#       Обновление данных
@app.route('/api/v0.2.0/user.config.update', methods=['POST'])
def update_user_config():
    # Проверка на получение данных в POST
    if not request.data:
        return error.no_data()
    # Проверка на авторизацию в API
    if not check.token(request.json['app_version'], request.json['token_hash']):
        return error.c401()
    # Проверка токена
    user_id = check.api_token(request.json['app_id'], request.json['token'])
    if False == user_id or None == user_id:
        return error.c401()
    # Проверка соответствия пользователя
    if not user_id == request.json['user_id']:
        return error.c403()
    # Проверка подтверждения регистрации
    if not check.user_verified(user_id):
        return error.user_unverified()
    # Поиск настроек пользователя по id
    user_config = UsersConfig.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_config or None == user_config:
        return error.c404()
    # FIXME: Костыль для обработки непоступивших значений для обновления
    new_data=False
    if request.json['theme'] is not None:
        theme = request.json['theme']
        new_data = True
    else:
        theme = user_config.theme
    if request.json['locale'] is not None:
        locale = request.json['locale']
        new_data = True
    else:
        locale = user_config.locale
    # Проверка на получение новых данных
    if not new_data:
        return error.no_data()
    user_config = UsersConfig(user_id=user_id,
                              theme=theme,
                              locale=locale)
    db.session.add(user_config)
    db.session.commit()
    # Подготовка данных для ответа
    user_config = UsersConfig.query.get(user_id)
    # Проверка на получение данных из БД
    if False == user_config or None == user_config:
        return error.main()
    data = {
        "theme": user_config.theme,
        "locale": user_config.locale
    }
    # Отправка ответа
    return success.config_update(data)

#   Метод
@app.route('/api/v0.2.0/user.contacts.get', methods=['POST'])
def get_user_contacts():
    return error.c501()

#   Метод
@app.route('/api/v0.2.0/user.contacts.get', methods=['POST'])
def get_user_contacts():
    return error.c501()


#   Прочие методы
@app.route('/api/v0.2.0/<method>', methods=['POST, GET, PUT, DELETE'])
def unknown_method(method):
    if method is not None:
        return error.c404()
    return error.c404()
