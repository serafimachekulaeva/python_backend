from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from functions import Validate

# Инициализируем класс валидации
check = Validate()

# Форма входа в adminCP
class EmployerLoginForm(FlaskForm):
    email = StringField('E-mail', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    save_session = BooleanField('Remember me')
    submit = SubmitField('Sign in')


# Форма регистрации в adminCP
class EmployerRegistrationForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    middle_name = StringField('Middle Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('E-mail', validators=[DataRequired(), Email()])
    phone_number = StringField('Phone number', validators=[DataRequired()])
    departament_id = StringField('Departament ID', validators=[DataRequired()])
    group_id = StringField('Group ID (access)', validators=[DataRequired()])
    is_read_only = BooleanField('Read only employer')
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Password again', validators=[DataRequired(), EqualTo(password)])
    submit = SubmitField('Register')

    def validate_email(self, email):
        if check.email_exists(email) is not None and check.email_exists(email) is not False:
            raise ValidationError('Please use a different e-mail')

    def validate_phone_number(self, phone_number):
        if check.email_exists(phone_number) is not None and check.email_exists(phone_number) is not False:
            raise ValidationError('Please use a different phone number')
