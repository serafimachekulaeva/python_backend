# coding=utf-8
from datetime import datetime
from app import db


# Примеси
#   Примесь времени
class TimeMixin(object):
    created_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, index=True, onupdate=datetime.utcnow())


class MessageMixin(object):
    text = db.Column(db.Text, nullable=False,
                     comment='Текст сообщения')
    attachments_json = db.Column(db.Text,
                                 comment='JSON путей до приложений к сообщению')
    sent_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow())
    read_at = db.Column(db.DateTime, index=True, onupdate=datetime.utcnow())


# Системные таблицы
#   Страны
class Countries(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер страны')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия страны')
    regions = db.relationship('Regions', backref='id', lazy='dynamic')


#   Регионы
class Regions(TimeMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   comment='Внутренний номер региона')
    country_id = db.Column(db.SmallInteger, db.ForeignKey('countries.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер страны')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия региона')
    cities = db.relationship('Cities', backref='id', lazy='dynamic')


#   Города
class Cities(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер города')
    region_id = db.Column(db.SmallInteger, db.ForeignKey('regions.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер региона')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия города')
    banks = db.relationship('Banks', backref='id', lazy='dynamic')
    clients = db.relationship('Clients', backref='id', lazy='dynamic')
    companies = db.relationship('Companies', backref='id', lazy='dynamic')


#   Настройки системы
class Config(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True)
    key = db.Column(db.Text, unique=True, nullable=False,
                    comment='Ключ настройки')
    value = db.Column(db.Text, unique=True, nullable=False,
                      comment='Значение настройки')


#   Аналог strings.xml
class Strings(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    key = db.Column(db.Text, unique=True, nullable=False, comment='Ключ')
    ru = db.Column(db.Text, nullable=False, comment='Значение на русском языке')
    ru_RU = db.Column(db.Text, comment='Значение на российском русском языке')
    en = db.Column(db.Text, nullable=False, comment='Значение на английском языке')
    en_US = db.Column(db.Text, comment='Значение на американском английском языке')
    en_GB = db.Column(db.Text, comment='Значение на британском английском языке')


#   Таблица веб-сессий для сотрудников
class EmployersSessions(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Порядковый номер сессии')
    session_id = db.Column(db.Text, index=True, nullable=False,
                           comment='Идентификатор сессии')
    session_data = db.Column(db.Text, index=True, nullable=False,
                             comment='Данные сессии')


#   Таблица веб-сессий для пользователей
class UsersSessions(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Порядковый номер сессии')
    session_id = db.Column(db.Text, index=True, nullable=False,
                           comment='Идентификатор сессии')
    session_data = db.Column(db.Text, index=True, nullable=False,
                             comment='Данные сессии')


# Таблицы сотрудников
#   Данные для логина сотрудников
class Employers(TimeMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   comment='Внутренний номер сотрудника')
    email = db.Column(db.Text, unique=True, index=True, nullable=False,
                      comment='Электронная почта (логин)')
    password = db.Column(db.Text, nullable=False,
                         comment='SHA256-хеш пароля сотрудника')
    last_passwords_sha256_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                                           comment='JSON с 10-ю SHA256-хешами хешей паролей')
    first_ipv4 = db.Column(db.Text,
                           comment='IPv4 адрес первого входа сотрудника')
    first_ua = db.Column(db.Text,
                         comment='User-agent первого входа сотрудника')
    is_email_verified = db.Column(db.Boolean, nullable=False, default=False,
                                  comment='Статус подтверждения электронной почты')
    is_read_only = db.Column(db.Boolean, nullable=False, default=True,
                             comment='Невозможность менять данные пользователя')
    departments = db.relationship('EmployersDepartments', backref='id', lazy='dynamic')
    actions = db.relationship('EmployersActionsLog', backref='id', lazy='dynamic')


#   Список отделов сотрудников
class EmployersDepartments(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер отдела')
    head_id = db.Column(db.Integer, db.ForeignKey('employers.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер сотрудника-главы отдела')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия отдела')
    description_key = db.Column(db.Text, unique=True, nullable=False,
                                comment='Ключ описания отдела')
    employers = db.relationship('EmployersProfiles', backref='id', lazy='dynamic')


#   Список ролей сотрудников
class EmployersRoles(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер роли')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия роли')
    is_active = db.Column(db.Boolean, nullable=False, default=False,
                          comment='Доступ к интранету')
    admin_cp = db.Column(db.Boolean, nullable=False, default=False,
                         comment='Доступ к панели администратора')
    mod_cp = db.Column(db.Boolean, nullable=False, default=False,
                       comment='Доступ к панели модератора')
    support_cp = db.Column(db.Boolean, nullable=False, default=False,
                           comment='Доступ к панели поддержки')
    is_read_only = db.Column(db.Boolean, nullable=False, default=True,
                             comment='Невозможность менять данные пользователя')
    employers = db.relationship('EmployersProfiles', backref='id', lazy='dynamic')


#   Данные профилей сотрудников
class EmployersProfiles (TimeMixin, db.Model):
    id = db.Column(db.Integer, db.ForeignKey('employers.id'), primary_key=True,
                   comment='Внутренний номер сотрудника')
    first_name = db.Column(db.Text, nullable=False,
                           comment='Имя')
    middle_name = db.Column(db.Text,
                            comment='Отчество')
    last_name = db.Column(db.Text, nullable=False,
                          comment='Фамилия')
    corp_email = db.Column(db.Text, unique=True, nullable=False,
                           comment='Корпоративный адрес электронной почты')
    phone_number = db.Column(db.BigInteger, nullable=False,
                             comment='Контактный номер телефона')
    pic = db.Column(db.Text, nullable=False, default='',  # TODO: Путь до дефолтного аватара
                    comment='Путь до аватара сотрудника')
    thumbs = db.Column(db.Text, nullable=False, default='',  # TODO: Путь до эскиза дефолтного аватара
                       comment='Путь до эскиза аватара сотрудника')
    department_id = db.Column(db.SmallInteger, db.ForeignKey('employers_departments.id'),
                              nullable=False,
                              comment='Внутренний номер отдела')
    role_id = db.Column(db.SmallInteger, db.ForeignKey('employers_roles.id'),
                        nullable=False,
                        comment='Внутренний номер роли')


#  Список возможных действий сотрудников
class EmployersActions(db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   comment='Внутренний номер действия сотрудника')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия действия')
    description_key = db.Column(db.Text, unique=True, nullable=False,
                                comment='Ключ описания действия')


#  Лог действий сотрудников
class EmployersActionsLog(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Порядковый номер действия')
    employer_id = db.Column(db.Integer, db.ForeignKey('employers.id'),
                            index=True, nullable=False,
                            comment='Внутренний номер сотрудника')
    ua = db.Column(db.Text, nullable=False,
                   comment='User-agent при выполнении действия')
    action_id = db.Column(db.Integer, db.ForeignKey('employers_actions.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер действия')


#  Логи входа сотрудников
class EmployersLoginLogs(db.Model):
    employer_id = db.Column(db.Integer, db.ForeignKey('employers.id'), primary_key=True,
                            comment='Внутренний номер сотрудника')
    last_ipv4 = db.Column(db.Text, nullable=False,
                          comment='IPv4 адрес последнего входа сотрудника')
    last_ua = db.Column(db.Text, nullable=False,
                        comment='User-agent последнего входа сотрудника')
    last_login_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow(),
                              comment='Время последнего входа в систему')
    last_ipv4_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                               comment='JSON десяти последних IPv4 адресов сотрудника')
    last_ua_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                             comment='JSON десяти последних User-agent сотрудника')
    last_login_at_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                                   comment='JSON десяти последних записей времени входа сотрудника')


# Таблицы пользователей
#   Данные для логина пользователей
class Users(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер пользователя')
    email = db.Column(db.Text, unique=True, index=True, nullable=False,
                      comment='Электронная почта (логин)')
    phone_number = db.Column(db.BigInteger, unique=True, index=True, nullable=False,
                             comment='Номер телефона (логин)')
    password = db.Column(db.Text, nullable=False,
                         comment='SHA256-хэш пароля пользователя')
    last_passwords_sha256_json = db.Column(db.Text, nullable=False,
                                           comment='JSON с 10-ю SHA256 хешами хешей пароля')
    first_ipv4 = db.Column(db.Text, index=True,
                           comment='IPv4 адрес первого входа пользователя')
    first_ua = db.Column(db.Text,
                         comment='User-agent первого входа пользователя')
    is_email_verified = db.Column(db.Boolean, nullable=False, default=False,
                                  comment='Статус подтверждения электронной почты (логина)')
    is_phone_number_verified = db.Column(db.Boolean, nullable=False, default=False,
                                         comment='Статус подтверждения номера телефона (логина)')
    is_company = db.Column(db.Boolean, nullable=False, default=False,
                           comment='Является ли компанией')
    actions = db.relationship('UsersActionsLog', backref='id', lazy='dynamic')
    api_tokens = db.relationship('UsersApiTokens', backref='id', lazy='dynamic')


#   Данные профилей пользователей
class UsersProfiles(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, db.ForeignKey('users.id'), primary_key=True,
                   comment='Внутренний номер пользователя')
    display_name = db.Column(db.Text, nullable=False,
                             comment='Отображаемое имя пользователя')
    pic = db.Column(db.Text, nullable=False, default='',  # TODO: Путь до дефолтного аватара
                    comment='Путь до аватара пользователя')
    thumbs = db.Column(db.Text, nullable=False, default='',  # TODO: Путь до эскиза дефолтного аватара
                       comment='Путь до эскиза аватара пользователя')
    bio = db.Column(db.Text,
                    comment='Поле «О себе»')
    is_data_verified = db.Column(db.Boolean, index=True, nullable=False, default=False,
                                 comment='Статус проверки личных данных')
    tests_and_certificates = db.relationship('TestsAndCertificates', backref='id', lazy='dynamic')
    portfolios = db.relationship('Portfolios', backref='id', lazy='dynamic')


#   Элементы портфолио
class Portfolios(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер элемента портфолио')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    name = db.Column(db.Text, nullable=False,
                     comment='Название элемента портфолио')
    description = db.Column(db.Text, nullable=False,
                            comment='Описание элемента портфолио')
    pic = db.Column(db.Text, nullable=False,
                    comment='Путь до изображения элемента портфолио')
    thumbs = db.Column(db.Text, nullable=False,
                       comment='Путь до эскиза изображения элемента портфолио')


#   Контакты пользователя
class UsersContacts(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                   index=True, nullable=False,
                   comment='Внутренний номер пользователя')
    phone_number = db.Column(db.BigInteger, unique=True, index=True,
                             comment='Контактный номер телефона')
    email = db.Column(db.Text, unique=True, index=True,
                      comment='Контактный адрес электронной почты')


#   Тесты и сертификаты на уровень владения навыками
class TestsAndCertificates(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер теста/сертификата')
    is_test = db.Column(db.Boolean, index=True, nullable=False, default=True,
                        comment='Является ли тестом или сертификатом')
    name_key = db.Column(db.Text, unique=True, index=True, nullable=False,
                         comment='Ключ названия теста/сертификата')
    description_key = db.Column(db.Text, unique=True, index=True,
                                comment='Ключ описания теста/сертификата')


#   Результаты прохождения тестов и сертификаты пользователей
class UsersTestsAndCertificates(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер результатов теста/записи о сертификате')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    internal_id = db.Column(db.BigInteger, db.ForeignKey('tests_and_certificates.id'),
                            index=True, nullable=False,
                            comment='Внутренний номер теста/сертификата')
    max_grade = db.Column(db.Double,
                          comment='Максимальная оценка по тесту')
    last_grade = db.Column(db.Double,
                           comment='Оценка последнего прохождения теста')
    attempts = db.Column(db.SmallInteger,
                         comment='Количество попыток прохождения теста')


# Настройки элементов рейтинга доверия
class TrustRateConfig(TimeMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.Text, index=True, unique=True, nullable=False,
                    comment='Ключ элемента рейтинга доверия')
    coefficient = db.Column(db.Double, nullable=False, default=0.00,
                            comment='Коэффициент элемента рейтинга доверия')


#   Элементы рейтинга доверия
class TrustRates(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер элемента рейтинга')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    response_speed = db.Column(db.Double, index=True, nullable=False,
                               comment='Средняя оценка скорости ответа')
    sociability = db.Column(db.Double, index=True, nullable=False,
                            comment='Средняя оценка общительности')
    quality = db.Column(db.Double, index=True, nullable=False,
                        comment='Средняя оценка качества работы/составления ТЗ')
    deadlines = db.Column(db.Double, index=True, nullable=False,
                          comment='Средняя оценка соблюдения сроков')
    disputes_count = db.Column(db.Double, index=True, nullable=False,
                               comment='Количество споров с участием пользователя')
    disputes_started = db.Column(db.Double, index=True, nullable=False,
                                 comment='Количество споров начатых пользователем')
    disputes_lost = db.Column(db.Double, index=True, nullable=False,
                              comment='Количество споров проигранных пользователем')
    profile_completion = db.Column(db.Double, index=True, nullable=False,
                                   comment='Процент заполнения профиля')
    property_value = db.Column(db.Double, index=True,
                               comment='Примерная рыночная стоимость имущества')
    credit_score = db.Column(db.Double, index=True,
                             comment='Кредитный рейтинг')


#   Настройки пользователя
class UsersConfig(TimeMixin, db.Model):
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    theme = db.Column(db.SmallInteger, nullable=False, default=0,
                      comment='Номер темы интерфейса')
    locale = db.Column(db.Text(8), nullable=False, default='ru',
                       comment='Локаль (язык)')


#  Список возможных действий пользователей
class UsersActions(db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   comment='Внутренний номер действия пользователя')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия действия')
    description_key = db.Column(db.Text, unique=True, nullable=False,
                                comment='Ключ описания действия')
    is_important = db.Column(db.Boolean, index=True, nullable=False,
                             comment='Является ли важным действием')


#   Лог действий пользователей
class UsersActionsLog(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Порядковый номер действия')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    ua = db.Column(db.Text, nullable=False,
                   comment='User-agent при выполнении действия')
    action_id = db.Column(db.Integer, db.ForeignKey('users_actions.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер действия')


#   Логи входа пользователей
class UsersLoginLogs(TimeMixin, db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True,
                        comment='Внутренний номер пользователя')
    last_ipv4 = db.Column(db.Text, nullable=False,
                          comment='IPv4 адрес последнего входа пользователя')
    last_ua = db.Column(db.Text, nullable=False,
                        comment='User-agent последнего входа пользователя')
    last_login_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow(),
                              comment='Время последнего входа в систему')
    last_ipv4_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                               comment='JSON десяти последних IPv4 адресов пользователя')
    last_ua_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                             comment='JSON десяти последних User-agent пользователя')
    last_login_at_json = db.Column(db.Text, nullable=False, default='{"count":0,"data":[]}',
                                   comment='JSON десяти последних записей времени входа пользователя')

#   Токены достпа к API
class UsersApiTokens(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Порядковый номер токена')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    app_id = db.Column(db.Text, nullable=False, unique=True,
                       comment='Идентификатор приложения')
    token = db.Column(db.Text, nullable=False,
                      comment='Токен доступа')
    expires_at = db.Column(db.DateTime, index=True, nullable=False,
                           default=(datetime.utcnow() + 60 * 60 * 24 * 28),
                           onupdate=(datetime.utcnow() + 60 * 60 * 24 * 28),
                           comment='Время окончания действия токена (28 суток)')


#   Таблица для подтверждения адреса электронной почты
class UserVerifyEmail(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    email = db.Column(db.Text, index=True, nullable=False, unique=True,
                      comment='Адрес электронной почты для подтверждения')
    code = db.Column(db.Text, nullable=False,
                     comment='Код подтверждения')
    created_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow())
    expires_at = db.Column(db.DateTime, index=True, nullable=False, default=(datetime.utcnow() + 60 * 60 * 24),
                           comment='Время истечения кода (24 часа)')


#   Таблица для подтверждения номера телефона
class UserVerifyPhone(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    phone_number = db.Column(db.BigInteger, index=True, nullable=False, unique=True,
                      comment='Номер телефона для подтверждения')
    code = db.Column(db.Text, nullable=False,
                     comment='Код подтверждения')
    created_at = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow())
    expires_at = db.Column(db.DateTime, index=True, nullable=False, default=(datetime.utcnow() + 60 * 5),
                           comment='Время истечения кода (5 минут)')

#   Заказы связанные с пользователями
class UsersOrders(TimeMixin, db.Model):
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'), primary_key=True,
                        comment='Внутренний номер пользователя')
    finished_orders_json = db.Column(db.Text, nullable=False,
                                     comment='Заказы выполненные пользователем')
    closed_orders_json = db.Column(db.Text, nullable=False,
                                   comment='Закрытые заказы созданные пользователем')
    open_orders_json = db.Column(db.Text, nullable=False,
                                 comment='Открытые заказы созданные пользователем')
    current_orders_json = db.Column(db.Text, nullable=False,
                                    comment='Заказы выполняемые пользователем')
    expected_orders_json = db.Column(db.Text, nullable=False,
                                     comment='Заказы на которые пользователь откликнулся')


# Вакансии и резюме
#   Графики работы
class Schedules(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер расписания')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия расписания')
    description_key = db.Column(db.Text, unique=True,
                                comment='Ключ описания расписания')


#   Вакансии
class Careers(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер вакансии')
    company_id = db.Column(db.BigInteger, db.ForeignKey('companies.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер компании/ИП')
    internal_scopes = db.Column(db.Text, nullable=False,
                                comment='Битовая маска вида вакансии(заказа)')
    city_id = db.Column(db.BigInteger, db.ForeignKey('cities.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер города с вакансией')
    salary_max_units = db.Column(db.Double, nullable=False,
                                 comment='Минимальная заработная плата в юнитах')
    salary_min_units = db.Column(db.Double, nullable=False,
                                 comment='Минимальная заработная плата в юнитах')
    schedule_id = db.Column(db.SmallInteger, db.ForeignKey('schedule.id'),
                            index=True, nullable=False,
                            comment='Внутренний номер расписания')
    min_experience = db.Column(db.SmallInteger, nullable=False,
                               comment='Минимальный опыт работы в месяцах')
    responders_json = db.Column(db.Text, nullable=False,
                                comment='JSON заинтересовашихся вакансией')
    comments = db.relationship('CareersComments', backref='id', lazy='dynamic')
    messages = db.relationship('CareersMessages', backref='id', lazy='dynamic')

#   Резюме
class Cvs(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер резюме')
    client_id = db.Column(db.BigInteger, db.ForeignKey('clients.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер клиента')
    internal_scopes = db.Column(db.Text, nullable=False,
                                comment='Битовая маска вида резюме (заказа)')
    cities_ids_json = db.Column(db.Text, nullable=False,
                                comment='JSON городов доступных соискателю')
    salary_max_units = db.Column(db.Double, nullable=False,
                                 comment='Минимальная заработная плата в юнитах')
    salary_min_units = db.Column(db.Double, nullable=False,
                                 comment='Минимальная заработная плата в юнитах')
    experience = db.Column(db.SmallInteger, nullable=False,
                           comment='Опыт работы в месяцах')
    offers_json = db.Column(db.Text, nullable=False,
                            comment='JSON предлагаемых вакансий')
    comments = db.relationship('CvsComments', backref='id', lazy='dynamic')

#   Комментарии вакансий
class CareersComments(TimeMixin. db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер комментария')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('careers_comments.id'),
                             index=True,
                             comment='Внутренний номер родительского комментария')
    career_id = db.Column(db.BigInteger, db.ForeignKey('careers.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер вакансии')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    text = db.Column(db.Text, nullable=False,
                     comment='Текст комментария')
    answers = db.relationship('CareersComments', backref='id', lazy='dynamic')

#   Комментарии резюме
class CvsComments(TimeMixin. db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер комментария')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('cvs_comments.id'),
                             index=True,
                             comment='Внутренний номер родительского комментария')
    cv_id = db.Column(db.BigInteger, db.ForeignKey('cvs.id'),
                      index=True, nullable=False,
                      comment='Внутренний номер резюме')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    text = db.Column(db.Text, nullable=False,
                     comment='Текст комментария')
    answers = db.relationship('CvsComments', backref='id', lazy='dynamic')

#   Сообщения вакансий
class CareersMessages(MessageMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер комментария')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('careers_messages.id'),
                             index=True,
                             comment='Внутренний номер родительского комментария')
    career_id = db.Column(db.BigInteger, db.ForeignKey('careers.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер вакансии')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')

# Средства (юниты)
#   Движения юнитов
class UnitsFlow(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер транзакции')
    units = db.Column(db.Double, nullable=False,
                      comment='Объём транзакции в юнитах')
    source_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер пользователя')
    destination_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                               index=True, nullable=False,
                               comment='Внутренний номер пользователя')
    status = db.Column(db.SmallInteger, index=True, nullable=False,
                       comment='Статус транзакции')
    source_balance_before = db.Column(db.Double, nullable=False,
                                      comment='Баланс пользователя-отправителя до транзакции в юнитах')
    source_balance_after = db.Column(db.Double, nullable=False,
                                     comment='Баланс пользователя-отправителя после транзакции в юнитах')
    destination_balance_before = db.Column(db.Double, nullable=False,
                                           comment='Баланс пользователя-получателя до транзакции в юнитах')
    destination_balance_after = db.Column(db.Double, nullable=False,
                                          comment='Баланс пользователя-получателя после транзакции в юнитах')


# Договоры и данные для их заключения
#   Реквизиты банков
class Banks(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер банка')
    bik = db.Column(db.Text, unique=True, nullable=False,
                    comment='БИК банка')
    swift = db.Column(db.Text, unique=True, nullable=False,
                      comment='SWIFT банка')
    iban = db.Column(db.Text, unique=True, nullable=False,
                     comment='IBAN банка')
    corr_acc = db.Column(db.Text, unique=True, nullable=False,
                         comment='Корреспондентский счёт банка')
    name_global = db.Column(db.Text, unique=True, nullable=False,
                            comment='Международное название банка')
    name_rus = db.Column(db.Text, unique=True,
                         comment='Русское название банка')
    clients = db.relationship('Clients', backref='id', lazy='dynamic')
    companies = db.relationship('Companies', backref='id', lazy='dynamic')


#   Личные данные физических лиц
class Clients(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер клиента')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    first_name = db.Column(db.Text, nullable=False,
                           comment='Имя')
    middle_name = db.Column(db.Text,
                            comment='Отчество')
    last_name = db.Column(db.Text, nullable=False,
                          comment='Фамилия')
    is_male = db.Column(db.Boolean, nullable=False, default=True,
                        comment='Является ли мужчиной')
    tin = db.Column(db.Text, nullable=False,
                    comment='ИНН (Taxpayer Identification Number)')
    bank_giro = db.Column(db.Text, nullable=False,
                          comment='Расчётный счёт в банке')
    bank_id = db.Column(db.BigInteger, db.ForeignKey('banks.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер банка')
    citizenship = db.Column(db.SmallInteger, db.ForeignKey('countries.id'),
                            index=True, nullable=False,
                            comment='Гражданство')
    id_number = db.Column(db.Text, unique=True, nullable=False,
                          comment='Серия и номер документа удостоверяющего личность')
    id_issue_date = db.Column(db.Date, nullable=False,
                              comment='Дата выдачи документа удостоверяющего личность')
    id_authority = db.Column(db.Text, nullable=False,
                             comment='Орган выдачи документа удостоверяющего личность')
    birth_date = db.Column(db.Date, nullable=False,
                           comment='Дата рожденеия')
    address_postal = db.Column(db.Text, nullable=False,
                               comment='Почтовый (физический) адрес')
    address_legal = db.Column(db.Text, nullable=False,
                              comment='Адрес прописки')
    city_id = db.Column(db.Text, db.ForeignKey('cities.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер города')


#   Реквизиты юридических лиц
class Companies(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер компании/ИП')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    type = db.Column(db.Text, index=True, nullable=False,
                     comment='Тип юридического лица')
    name_global = db.Column(db.Text, unique=True, nullable=False,
                            comment='Муждународное название компании/ИП')
    name_rus = db.Column(db.Text, unique=True,
                         comment='Русское название компании/ИП')
    tin = db.Column(db.Text, nullable=False,
                    comment='ИНН (Taxpayer Identification Number)')
    trrc = db.Column(db.Text, nullable=False,
                     comment='КПП (Tax Registration Reason Code)')
    psrn = db.Column(db.Text, nullable=False,
                     comment='ОГРН[ИП] (Primary State Registration Number [of the Sole Proprietor])')
    bank_giro = db.Column(db.Text, nullable=False,
                          comment='Расчётный счёт в банке')
    bank_id = db.Column(db.BigInteger, db.ForeignKey('banks.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер банка')
    address_postal = db.Column(db.Text, nullable=False,
                               comment='Почтовый (физический) адрес')
    address_legal = db.Column(db.Text, nullable=False,
                              comment='Юридический адрес')
    city_id = db.Column(db.Text, db.ForeignKey('cities.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер города')
    contacts = db.relationship('CompaniesContacts', backref='id', lazy='dynamic')


#   Контактные лица компаний/ИП
class CompaniesContacts(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер контактного лица')
    company_id = db.Column(db.BigInteger, db.ForeignKey('companies.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер компании/ИП')
    first_name = db.Column(db.Text, nullable=False,
                           comment='Имя')
    middle_name = db.Column(db.Text,
                            comment='Отчество')
    last_name = db.Column(db.Text, nullable=False,
                          comment='Фамилия')
    position = db.Column(db.Text, nullable=False,
                         comment='Должность в компании/ИП')
    email = db.Column(db.Text, nullable=False,
                      comment='Контактный адрес электронной почты')
    phone_number = db.Column(db.Text, nullable=False,
                             comment='Контактный номер телефона')


#   Настройки подписок
class Subscriptions(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер подписки')
    name_key = db.Column(db.Text, unique=True, nullable=False,
                         comment='Ключ названия подписки')
    duration = db.Column(db.Integer, nullable=False,
                         comment='Длительность периода в секундах')
    max_periods = db.Column(db.SmallInteger, nullable=False,
                            comment='Максимальное число периодов')
    cost_upp = db.Column(db.Double, nullable=False,
                         comment='Стоимость за период, в юнитах')
    max_upo = db.Column(db.Double, nullable=False,
                        comment='Максимальная стоимость за заказ, в юнитах')
    max_opp = db.Column(db.Integer, nullable=False, default=-1,
                        comment='Максимальное число заказов за период')


#   Договоры
class Contracts(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    number = db.Column(db.Text, unique=True, nullable=False,
                       comment='Номер договора')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    subscription_id = db.Column(db.SmallInteger, db.ForeignKey('subscriptions.id'),
                                index=True, nullable=False,
                                comment='Внутренний номер подписки')
    periods = db.Column(db.SmallInteger, nullable=False,
                        comment='Длительнось договора, в периодах')
    cost_multiply = db.Column(db.Double, nullable=False,
                              comment='Коэффициент умножения UPP')
    agent_comm = db.Column(db.Double, nullable=False,
                           comment='Агентская комиссия, в процентах')


# Заказы
#   Разделы заказов
class GlobalScopes(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер раздела')
    mask = db.Column(db.Text, index=True,  unique=True, nullable=False,
                     comment='Битовая маска раздела')
    name_key = db.Column(db.Text, unique=True, index=True, nullable=False,
                         comment='Ключ названия раздела')
    description_key = db.Column(db.Text, unique=True, index=True,
                                comment='Ключ описания раздела')


#   Виды заказов
class InternalScopes(TimeMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   comment='Внутренний номер вида')
    global_scope_id = db.Column(db.SmallInteger, db.ForeignKey('global_scopes.id'),
                                index=True, nullable=False,
                                comment='Внутренний номер раздела')
    mask = db.Column(db.Text, index=True,  unique=True, nullable=False,
                     comment='Битовая маска вида')
    name_key = db.Column(db.Text, unique=True, index=True, nullable=False,
                         comment='Ключ названия вида')
    description_key = db.Column(db.Text, unique=True, index=True,
                                comment='Ключ описания вида')


#  Заказы
class Orders(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер заказа')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-заказчика')
    executor_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                            index=True, nullable=False,
                            comment='Внутренний номер пользователя-исполнителя')
    responders_json = db.Column(db.Text, nullable=False,
                                comment='JSON внутренних номеров откликнувшихся пользователей')
    status = db.Column(db.SmallInteger, nullable=False, default=0,
                       comment='Статус заказа')
    name = db.Column(db.Text, nullable=False,
                     comment='Название заказа')
    description = db.Column(db.Text, nullable=False,
                            comment='Описание заказа')
    technical_scope = db.Column(db.Text,
                                comment='Путь до технического задания')
    units_cost = db.Column(db.Double, nullable=False,
                           comment='Вознаграждение в юнитах')
    order_duration = db.Column(db.Integer, nullable=False,
                               comment='Время на выполнение заказа в секундах')
    internal_scopes = db.Column(db.Text, nullable=False,
                                comment='Битовая маска видов заказов')
    is_offline = db.Column(db.Boolean, nullable=False, default=False,
                           comment='Является ли оффлайн-заказом (с геопозициями)')
    order_geos_json = db.Column(db.Text,
                                comment='JSON геопозиций заказа')
    executor_geo = db.Column(db.Text,
                             comment='Геопозиция исполнителя')
    geo_updated_at = db.Column(db.DateTime,
                               comment='Время обновления геопозиции исполнителя')
    status_updated_at = db.Column(db.DateTime,
                                  comment='Время обновления статуса заказа')
    comments = db.relationship('OrdersComments', backref='id', lazy='dynamic')
    messages = db.relationship('OrdersMessages', backref='id', lazy='dynamic')


#   Услуги
class ExecutorsServices(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер услуги')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер исполнителя')
    orders_json = db.Column(db.Text, nullable=False,
                            comment='JSON предложенных заказов')
    status = db.Column(db.SmallInteger, nullable=False, default=0,
                       comment='Статус [видимости] услуги')
    name = db.Column(db.Text, nullable=False,
                     comment='Название услуги')
    description = db.Column(db.Text, nullable=False,
                            comment='Описание услуги')
    units_lowest_price = db.Column(db.Double, nullable=False,
                                   comment='Минимальное вознаграждение за заказ в юнитах')
    units_highest_price = db.Column(db.Double, nullable=False, default=-1,
                                    comment='Максимальное вознаграждение за заказ в юнитах')
    max_order_duration = db.Column(db.Integer, nullable=False,
                                   comment='Максимальная длительность заказа')
    internal_scopes = db.Column(db.Text, nullable=False,
                                comment='Битовая маска видов заказов')
    comments = db.relationship('ExecutorsServicesComments', backref='id', lazy='dynamic')


#   Комментарии заказов
class OrdersComments(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер комментария')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('orders_comments.id'),
                             index=True,
                             comment='Внутренний номер родительского комментария')
    order_id = db.Column(db.BigInteger, db.ForeignKey('orders.id'),
                         index=True, nullable=False,
                         comment='Внутренний номер заказа')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    text = db.Column(db.Text, nullable=False,
                     comment='Текст комментария')
    answers = db.relationship('OrdersComments', backref='id', lazy='dynamic')


#   Комментарии услуг
class ExecutorsServicesComments(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер комментария')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('executors_services_comments.id'),
                             index=True,
                             comment='Внутренний номер родительского комментария')
    executor_service_id = db.Column(db.BigInteger, db.ForeignKey('executors_services.id'),
                                    index=True, nullable=False,
                                    comment='Внутренний номер услуги')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    text = db.Column(db.Text, nullable=False,
                     comment='Текст комментария')
    answers = db.relationship('ExecutorsServicesComments', backref='id', lazy='dynamic')


#   Сообщения заказа
class OrdersMessages(MessageMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер сообщения')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('orders_messages.id'),
                             index=True,
                             comment='Внутренний номер родительского сообщения')
    order_id = db.Column(db.BigInteger, db.ForeignKey('orders.id'),
                         index=True, nullable=False,
                         comment='Внутренний номер заказа')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    answers = db.relationship('OrdersMessages', backref='id', lazy='dynamic')


#   Отзывы к заказам
class OrdersReviews(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер отзыва')
    order_id = db.Column(db.BigInteger, db.ForeignKey('orders.id'),
                         index=True, nullable=False,
                         comment='Внутренний номер заказа')
    creator_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                           index=True, nullable=False,
                           comment='Внутренний номер пользователя-отправителя')
    target_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер оцениваемого пользователя')
    text = db.Column(db.Text, nullable=False,
                     comment='Текст отзыва')
    response_speed = db.Column(db.Double, index=True, nullable=False,
                               comment='Оценка скорости ответа')
    sociability = db.Column(db.Double, index=True, nullable=False,
                            comment='Оценка общительности')
    quality = db.Column(db.Double, index=True, nullable=False,
                        comment='Оценка качества работы/составления ТЗ')
    deadlines = db.Column(db.Double, index=True, nullable=False,
                          comment='Оценка соблюдения срока выполнения работы')


# Споры и техническая поддержка
#   Разделы технической поддержки
class SupportDepartments(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер раздела')
    name_key = db.Column(db.Text, unique=True, index=True, nullable=False,
                         comment='Ключ названия раздела')
    description_key = db.Column(db.Text, unique=True, index=True,
                                comment='Ключ описания раздела')


#   Подразделы технической поддержки
class SupportSubdepartments(TimeMixin, db.Model):
    id = db.Column(db.SmallInteger, primary_key=True,
                   comment='Внутренний номер подраздела')
    department_id = db.Column(db.SmallInteger, db.ForeignKey('support_departments.id'),
                              index=True, nullable=False,
                              comment='Внутренний номер раздела')
    name_key = db.Column(db.Text, unique=True, index=True, nullable=False,
                         comment='Ключ названия подраздела')
    description_key = db.Column(db.Text, unique=True, index=True,
                                comment='Ключ описания подраздела')


#   Споры
class Disputes(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер спора')
    order_id = db.Column(db.BigInteger, db.ForeignKey('orders.id'),
                         index=True, nullable=False,
                         comment='Внутренний номер заказа')
    moderator_id = db.Column(db.BigInteger, db.ForeignKey('employers.id'),
                             index=True, nullable=False,
                             comment='Внутренний номер сотрудника-модератора')
    status = db.Column(db.SmallInteger, nullable=False, default=0,
                       comment='Статус спора')
    creator_cost = db.Column(db.Double,
                             comment='Предложенное заказчиком вознаграждение')
    executor_cost = db.Column(db.Double,
                              comment='Предложенное исполнителем вознаграждение')
    is_creator_agree = db.Column(db.Boolean, nullable=False, default=False,
                                 comment='Согласие заказчика')
    is_executor_agree = db.Column(db.Boolean, nullable=False, default=False,
                                  comment='Согласие исполнителя')
    status_updated_at = db.Column(db.DateTime,
                                  comment='Время обновления статуса спора')
    messages = db.relationship('DisputesMessages', backref='id', lazy='dynamic')

#   Тикеты
class Tickets(TimeMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер тикета')
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'),
                        index=True, nullable=False,
                        comment='Внутренний номер пользователя')
    name = db.Column(db.Text, nullable=False,
                     comment='Название тикета')
    status = db.Column(db.SmallInteger, nullable=False, default=0,
                       comment='Статус спора')
    subdepartament_id = db.Column(db.SmallInteger, db.ForeignKey('support_subdepartments.id'),
                                  index=True, nullable=False,
                                  comment='Внутренний номер подраздела тикета')
    status_updated_at = db.Column(db.DateTime, index=True,
                                  comment='Время обновления статуса тикета')
    messages = db.relationship('TicketsMessages', backref='id', lazy='dynamic')


#   Сообщения споров
class TicketsMessages(MessageMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер сообщения')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('tickets_messages.id'),
                             index=True,
                             comment='Внутренний номер родительского сообщения')
    ticket_id = db.Column(db.BigInteger, db.ForeignKey('tickets.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер тикета')
    is_from_user = db.Column(db.Boolean, nullable=False, default=True,
                             comment='Является ли вопросом (от пользователя)')
    creator_id = db.Column(db.BigInteger, index=True, nullable=False,
                           comment='Внутренний номер отправителя (пользователя/сотрудника-модератора)')


#   Сообщения тикетов
class DisputesMessages(MessageMixin, db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   comment='Внутренний номер сообщения')
    answer_to_id = db.Column(db.BigInteger, db.ForeignKey('disputes_messages.id'),
                             index=True,
                             comment='Внутренний номер родительского сообщения')
    disput_id = db.Column(db.BigInteger, db.ForeignKey('disputes.id'),
                          index=True, nullable=False,
                          comment='Внутренний номер тикета')
    is_from_user = db.Column(db.Boolean, nullable=False, default=True,
                             comment='Является ли вопросом (от пользователя)')
    creator_id = db.Column(db.BigInteger, index=True, nullable=False,
                           comment='Внутренний номер отправителя (пользователя/сотрудника-модератора)')
