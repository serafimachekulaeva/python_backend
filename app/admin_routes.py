# -*- coding: utf-8 -*-

from flask import render_template, redirect, url_for
from app import app
from app.admin_forms import *
# TODO: Логин для администраторов, вкл. сессии, логаут. Пуши для них же


@app.route('/adminCP/registration/')
def employer_registration():
    return "There will be an a registrant form"


@app.route('/adminCP/login/', methods=['GET', 'POST'])
def employer_login():
    form = EmployerLoginForm()
    if form.validate_on_submit():
        return redirect(url_for("main_page"))
    return render_template("admincp-login.html", title="Sign In — adminCP", form=form)
