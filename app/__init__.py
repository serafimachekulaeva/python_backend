# coding=utf-8
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate

app = Flask(__name__, static_folder="static", template_folder="templates")  # Инициализируем приложение
app.config.from_object(Config)  # Получаем настройки приложения
db = SQLAlchemy(app)  # Инициализируем базу данных
ma = Marshmallow(app)  # Инициализируем схемы
mg = Migrate(app, db)  # Инициализируем миграцию

# Добавляем роутинг, модели и схемы
from app import api_routes, admin_routes, user_routes, models, schemas
