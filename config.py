import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or \
                 '8MbiC3GRAwLrSCW98hgubBqGU8TeEIKjLBMWus4X5VWt9Ne8RBM949fePMhh65Ab8G9SBfwY8uwnwcyhiqV8nnG7TSKkvpzZkGSbLv9zuKVPHK2P44NF2rI3Gz6JgVhZ'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or \
        'sqlite:///' + os.path.join(basedir, 'simplejob.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
